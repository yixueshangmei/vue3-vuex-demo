import { ref } from 'vue'
import { createStore } from 'vuex'
import axios from 'axios'

export default createStore({
  state: {
    counter: ref(0),
    colorCode: ref('blue')
  },
  mutations: {
    INCREASE_COUNTER(state, randomNumber) { // state, payload
      // console.log('randomNumber: ', randomNumber)
      state.counter += randomNumber
    },
    DECREASE_COUNTER(state, randomNumber) {
      state.counter -= randomNumber
    },
    SET_COLOR_CODE(state, newValue) {
      state.colorCode = newValue
    }
  },
  actions: {
    increase({ commit }) {
      // commit('INCREASE_COUNTER')
      axios('https://www.random.org/integers/?num=1&min=1&max=6&col=1&base=10&format=plain&rnd=new')
      .then(response => (
        // console.log('response: ', response)
        commit('INCREASE_COUNTER', response.data)
      ))
    },
    decrease({ commit }) {
      // commit('DECREASE_COUNTER')
      axios('https://www.random.org/integers/?num=1&min=1&max=6&col=1&base=10&format=plain&rnd=new')
      .then(response => (
        // console.log('response: ', response)
        commit('DECREASE_COUNTER', response.data)
      ))
    },
    setColorCode({ commit }, newValue) {
      commit('SET_COLOR_CODE', newValue)
    }
  },
  getters: {
    counterSquared(state) {
      return state.counter * state.counter
    }
  }
})
